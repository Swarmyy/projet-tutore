﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Projet_Tutoré
{
    class Tile
    {
        public string type;        
        public static int sea = 64;
        public static int forest = 32;
        public Dictionary<(int x, int y), bool> borders;        
        private static (int, int)[] cardinals = new (int, int)[] { (0,1),(1,0),(0,-1),(-1,0) };
        public Tile(int data)
        {
            //List<int> pow2 = new List<int>();
            borders = new Dictionary<(int x, int y), bool>();
            if (data > sea)
            {
                type = "sea";
                data -= sea;
            }
            else if (data > forest)
            {
                type = "forest";
                data -= forest;
            }
            else
            {
                type = "normal";
            }
            borders = Decode(data);
        }
        static Dictionary<(int x, int y), bool> Decode(int data)
        {
            bool isCardinal = false;
            Dictionary<(int x, int y), bool> rtn = new Dictionary<(int x, int y), bool>();
            int nCardinals = cardinals.Length;
            for (int i = 0; i < nCardinals; i++)
            {
                int power = nCardinals - i - 1;
                if (Convert.ToDouble(data) / Math.Pow(2, power) >= 1)
                {
                    isCardinal = true;
                    data -= Convert.ToInt32(Math.Pow(2, power));
                }
                rtn.Add(cardinals[cardinals.Length - i - 1], isCardinal);
                isCardinal = false;
            }
            return rtn;
        }
    }
}
