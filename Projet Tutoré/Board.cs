﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Projet_Tutoré
{
    class Board
    {
        public char[,] cBoard;
        public Tile[,] tileBoard;
        private int size;
        private static char normal=Convert.ToChar(2048);
        public Board(string strBoard, int size = 10)
        {
            this.size = size;
            int[,] intBoard = TranslateBoard(strBoard);
            tileBoard = new Tile[size, size];
            cBoard = new char[size, size];
            //on fait la map      
            int nParcell = 0;
            for (int nRow = 0; nRow < size; nRow++)
            {
                for (int nCol = 0; nCol < size; nCol++)
                {
                    tileBoard[nRow, nCol] = new Tile(intBoard[nRow, nCol]);
                    if (tileBoard[nRow, nCol].type == "sea")
                    {
                        cBoard[nRow, nCol] = 'M';
                    }
                    else if (tileBoard[nRow, nCol].type == "forest")
                    {
                        cBoard[nRow, nCol] = 'F';
                    }
                    else
                    {                        
                        cBoard[nRow, nCol] = normal;                        
                        bool isFrontier = false;
                        int xCard = 0;
                        int yAllow = 0;
                        int card = 0;
                        do
                        {
                            xCard = (card % 2) * (card - 2);
                            yAllow = ((card + 1) % 2) * (card - 1);
                            try
                            {
                                isFrontier = IsInitiated(cBoard[nRow + xCard, nCol + yAllow]) && !tileBoard[nRow, nCol].borders[(xCard, yAllow)];                                
                            }
                            catch (Exception ex) { }
                            card++;
                        } while (card < 4 && !isFrontier);
                        if (isFrontier)
                        {
                            cBoard[nRow, nCol] = cBoard[nRow + xCard, nCol + yAllow];                            
                        }
                        else
                        {

                            nParcell++;
                        }
                            
                    }
                }
            }
        }

        /**
         *  Convert a string to a 2 dimension int array
         *  Basic separator respectively for column and row are ':' and '|'
         *  params: string,[char,char]
         *  returns: int[,]
         */
        private int[,] TranslateBoard(string strBoard, char separatorColumn = ':', char separatorRow = '|')
        {
            int[,] intArray = new int[size, size];//return value,array of int representing the values of each case                       
            string[] rows = strBoard.Split(separatorRow);//array of each row
            //converting row by row
            for (int numberRow = 0; numberRow < size; numberRow++)
            {
                string[] columns = rows[numberRow].Split(separatorColumn);//array of each column in the row being studied
                //adding each integer in the string array to the int array
                for (int numberColumn = 0; numberColumn < size; numberColumn++)
                {
                    int.TryParse(columns[numberColumn], out intArray[numberRow, numberColumn]);
                }
            }
            return intArray;
        }

        /**
         * Test if the char is initiated ie it has a parcel
         * params: char
         * returns: bool
         */
        private bool IsInitiated(char cTile)
        {
            return cTile != normal && cTile != 'M' && cTile != 'F';
        }        
    }
}
