﻿using System;
using System.Collections.Generic;

namespace Projet_Tutoré
{
    class Program
    {             
        static void Main(string[] args)
        {           
            string scabb = "3:9:71:69:65:65:65:65:65:73|" +
                "2:8:3:9:70:68:64:64:64:72|" +
                "6:12:2:8:3:9:70:68:64:72|" +
                "11:11:6:12:6:12:3:9:70:76|" +
                "10:10:11:11:67:73:6:12:3:9|" +
                "14:14:10:10:70:76:7:13:6:12|" +
                "3:9:14:14:11:7:13:3:9:75|" +
                "2:8:7:13:14:3:9:6:12:78|" +
                "6:12:3:1:9:6:12:35:33:41|" +
                "71:77:6:4:12:39:37:36:36:44|";

            string phatt = "67:69:69:69:69:69:69:69:69:73|" +
                "74:3:9:7:5:13:3:1:9:74|" +
                "74:2:8:7:5:13:6:4:12:74|" +
                "74:6:12:7:9:7:13:3:9:74|" +
                "74:3:9:11:6:13:7:4:8:74|" +
                "74:6:12:6:13:11:3:13:14:74|" +
                "74:7:13:7:13:10:10:3:9:74|" +
                "74:3:9:11:7:12:14:2:8:74|" +
                "74:6:12:6:13:7:13:6:12:74|" +
                "70:69:69:69:69:69:69:69:69:76|";
            string current = scabb;
            foreach(string line in current.Split('|'))
            {
                foreach(string number in line.Split(':')){
                    Console.Write("{0}\t", number);
                }
                Console.WriteLine("");
            }
                
            Board boardCurrent = new Board(current);            

            int j = 0;
            foreach (char i in boardCurrent.cBoard)
            {
                if (j % 10 == 0)
                {
                    Console.WriteLine("");
                }
                Console.Write("{0}\t",i);
                j++;
            }
            
        }
    }
}
